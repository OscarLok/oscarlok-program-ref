package com.wsexample.service;

import java.util.List;

import com.wsexample.model.FulltextResult;
import com.wsexample.model.Pdfcontent;

public interface PdfcontentService {
	
	Pdfcontent getPdfcontentById(String id);
    
    List<FulltextResult> getPdfcontentByQuery(String keyword, String index );

    void addPdfcontent(List<Pdfcontent> pdfcontent, String index, String type);

    void updatePdfcontent(String updatecontent, String index, String type, String id);
    
    void removePdfcontentFields(String partoffields, String index, String type, String id);

    void deletePdfcontent(String index, String type, String id);
}
