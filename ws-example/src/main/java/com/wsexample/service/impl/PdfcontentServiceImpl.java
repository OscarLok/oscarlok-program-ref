package com.wsexample.service.impl;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.elasticsearch.index.query.QueryBuilders.*;

import org.apache.log4j.Logger;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHitField;
import org.elasticsearch.search.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.sort.SortParseElement;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wsexample.model.FulltextResult;
import com.wsexample.model.Pdfcontent;
import com.wsexample.service.PdfcontentService;
import com.wsexample.util.ValidatePdfcontent;


/**
 * @author LKW374
 *
 */

@Service
public class PdfcontentServiceImpl implements PdfcontentService {
	
	@Autowired
	private TransportClient client;
	private static final Logger LOGGER = Logger.getLogger(PdfcontentServiceImpl.class.getName());
	
/*	public PdfcontentServiceImpl (){
		try{
			client = TransportClient.builder().build()
		        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300));
		}catch (Exception e){
			LOGGER.debug("Failed to build the connection!");
			e.printStackTrace();
		}
	}*/
	
    public Pdfcontent getPdfcontentById (String id) {
    	LOGGER.info("getPdfcontentById services has been called!");
    	GetResponse response = client.prepareGet("test_v1","pdfcontent", id).get();
    	ObjectMapper mapper = new ObjectMapper();
    	Pdfcontent result = new Pdfcontent();
    	
    	if(!response.isExists()){
    		result = null;
    		return result;
    	}
    	
    	try {
			result = mapper.readValue(response.getSourceAsString(), Pdfcontent.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
        return result;
    }

    
    /**   (Search Pdfcontent by query) 
     * 
     * @param keyword, index @type String
     * @return List<FulltextResult>
     * 
     */
    public List<FulltextResult> getPdfcontentByQuery(String keyword, String index){
    	LOGGER.info("getPdfcontentByQuery services has been called!");
    	ObjectMapper mapper = new ObjectMapper();
    	QueryBuilder qb = matchQuery("file.content", keyword);
    	SearchResponse scrollResp = client.prepareSearch(index)
    									.setQuery(qb)
    									.addHighlightedField("file.content")
/*    									.setHighlighterNumOfFragments(0)*/ //this allow to return full text content with the highlight tag
    									.setHighlighterFragmentSize(500)
    									.setSize(100).execute().actionGet();
/*    	LOGGER.info("Result:" + scrollResp);*/
    	
    	SearchHit[] hits = scrollResp.getHits().getHits();
/*    	JSONParser parser = new JSONParser();*/
    	JSONObject obj = new JSONObject();
    	
    	FulltextResult pdfcontent = new FulltextResult();
		
		List<FulltextResult> result = new ArrayList<FulltextResult>();
			
    	if (scrollResp.getHits().getTotalHits() > 0 ){
    		try {
    			for (int i =0; i<hits.length; i++){
    				pdfcontent = mapper.readValue(hits[i].getSourceAsString(), FulltextResult.class);
    				String highlighttext = hits[i].highlightFields().get("file.content").fragments()[0].string(); 
    				pdfcontent.setId(hits[i].getId());
    				pdfcontent.setHighlightfield(highlighttext);
    				result.add(pdfcontent);
    			}				
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
	    		LOGGER.info("Cannot map from json string");
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
    	
    	return result;
    }

    
    /** (Add Pdfcontent Services)
     * 
     * @param pdfcontent:List<Pdfcontent> , index:String , type:String 
     * @return 
     *
     */
    public void addPdfcontent(List<Pdfcontent> pdfcontent, String index, String type) {
    	BulkRequestBuilder bulkRequest = client.prepareBulk();
    	ObjectMapper mapper = new ObjectMapper();
    	ValidatePdfcontent val = new ValidatePdfcontent();
    	
    	for (Pdfcontent pdf : pdfcontent){
    		if(!val.filenameValidator(index, pdf.getFilename())){
    			LOGGER.info("Filename is already exist!");
    			return;
    		}
    			
    		Map<String, Object> json = mapper.convertValue(pdf, Map.class);
        	try{
            	bulkRequest.add(client.prepareIndex(index, type)
            				.setSource(json)
            			);
            	} catch (Exception e){
            		// TODO Auto-generated catch block
    				e.printStackTrace();
            		LOGGER.info("Set Sources Failed");
            }
    	}
    	
    	
    	BulkResponse bulkResponse = bulkRequest.get();
    	if (bulkResponse.hasFailures()){
    		LOGGER.info("Bulk request failed");
    		try {
				throw new Exception (bulkResponse.buildFailureMessage());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
    }
    
    
	/** (Update Pdfcontent Services)
	 * 
	 * @param pdfcontent:Pdfcontent, index:String, type:String, id:String 
	 * @return
	 *
	 */
   public void updatePdfcontent(String updatecontent, String index, String type, String id) {
        UpdateRequest updateRequest = new UpdateRequest(index, type, id);
        IndexRequest indexRequest = new IndexRequest(index, type, id);
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> json = null;
		try {
			json = mapper.readValue(updatecontent, Map.class);
		} catch (JsonParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		indexRequest.source(json);
        updateRequest.doc(json).upsert(indexRequest);
        
        try {
			client.update(updateRequest).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
   
	/** (Update Pdfcontent Services)
	 * 
	 * @param partoffields:String, index:String, type:String, id:String 
	 * @return
	 *
	 */
   public void removePdfcontentFields(String partoffields, String index, String type, String id){

	   UpdateRequest updateRequest = new UpdateRequest(index, type, id);

		   List<String> lists = new ArrayList<String>(Arrays.asList(partoffields.split("\\s+")));
		   for(String removefield:lists){
			   updateRequest.script(new Script("ctx._source.remove(\"" + removefield.trim() + "\")"));
			   
			   try {
					client.update(updateRequest).get();
					LOGGER.info("The field "+ removefield.trim() + " in recordId: " + id.trim() + " has been removed.");
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		   }


   }
   
   /** (Delete Pdfcontent Services)
    * 
    * @param index:String, type:String, id:String
    * @return
    *
    */
    public void deletePdfcontent(String index, String type, String id) {
    	
    	DeleteResponse response = client.prepareDelete(index, type, id).get();
        
    }

}
