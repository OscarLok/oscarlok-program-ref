package com.wsexample.controller;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wsexample.model.FulltextResult;
import com.wsexample.model.Pdfcontent;
import com.wsexample.service.PdfcontentService;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/pdfcontent")
public class PdfcontentController {
	
	@Autowired
	private PdfcontentService pdfcontentservice;
	
	
	@RequestMapping(value = "/query/{recordId}", method = RequestMethod.GET)
	public ResponseEntity<Pdfcontent> getPdfById(@PathVariable("recordId") String recordId ){
		
		Pdfcontent pdfcontent = pdfcontentservice.getPdfcontentById(recordId);
		if(pdfcontent==null){
			 System.out.println("Pdf record with id " + recordId + " not found");
	         return new ResponseEntity<Pdfcontent>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Pdfcontent>(pdfcontent, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/query", method = RequestMethod.GET)
	public ResponseEntity<List<FulltextResult>> getPdfByQuery(@RequestParam("index") String index, @RequestParam("search") String keywords ){
		
		List<FulltextResult> result = pdfcontentservice.getPdfcontentByQuery(keywords, index);
		
		if(result.isEmpty()){
			 System.out.println("No record found with keyword " + keywords);
	         return new ResponseEntity<List<FulltextResult>>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<FulltextResult>>(result, HttpStatus.OK); 
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<String> addPdfcontent(@RequestBody List<Pdfcontent> pdfcontent, @RequestParam("index") String index, @RequestParam("type") String type){
	
		if(pdfcontent != null && !pdfcontent.isEmpty()){
			pdfcontentservice.addPdfcontent(pdfcontent, index, type);
		}else {
			 new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		
		
		return new ResponseEntity<String>("Add record request sent successfully.", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<String> updatePdfcontent(@RequestBody String updatecontent, @RequestParam("index") String index, @RequestParam("type") String type, @RequestParam("id") String id){
		
		Pdfcontent record = pdfcontentservice.getPdfcontentById(id);
		
		if(record == null){
            System.out.println("Unable to update. Pdfcontent with recordId " + id + " not found");
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		
		if (updatecontent != null && updatecontent != ""){
			pdfcontentservice.updatePdfcontent(updatecontent, index, type, id);
		}else {
			return new ResponseEntity<String>("Please enter the collect param.", HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<String>("Update record request sent successfully. ", HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/removefields", method = RequestMethod.PUT)
	public ResponseEntity<String> removePdfcontentFields(@RequestParam("fields") String partoffields, @RequestParam("index") String index, @RequestParam("type") String type, @RequestParam("id") String id){
		
		Pdfcontent record = pdfcontentservice.getPdfcontentById(id);
		
		if(record == null){
            System.out.println("Unable to remove field(s). Pdfcontent with recordId " + id + " not found");
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		
		if(partoffields != null && partoffields != ""){
			pdfcontentservice.removePdfcontentFields(partoffields, index, type, id);
		}else {
			return new ResponseEntity<String>("Please enter the collect param.",HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<String>("Remove Fields request sent successfully.",HttpStatus.OK);
		
	}
	
	
	@RequestMapping(value="/delete", method = RequestMethod.DELETE)
	public ResponseEntity<String> deletePdfcontent(@RequestParam("index") String index, @RequestParam("type") String type, @RequestParam("id") String id ){
		
		Pdfcontent record = pdfcontentservice.getPdfcontentById(id);
		
		if(record == null){
            System.out.println("Unable to delete record. Pdfcontent with recordId " + id + " not found");
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		
		pdfcontentservice.deletePdfcontent(index, type, id);
		
		return new ResponseEntity<String>("Delete record request sent successfully. ", HttpStatus.OK);
	}
}
