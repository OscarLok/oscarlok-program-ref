package com.wsexample.util;

import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

import java.net.InetAddress;

import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;


public class ValidatePdfcontent {

	private TransportClient client;
	private Boolean result;
	private static final Logger LOGGER = Logger.getLogger(ValidatePdfcontent.class.getName());
	
	public ValidatePdfcontent(){
		
		try{
			client = TransportClient.builder().build()
		        .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300));
		}catch (Exception e){
			LOGGER.debug("Failed to build the connection!");
			e.printStackTrace();
		}
	}
	
	public Boolean filenameValidator(String myindex, String filename) {
    	QueryBuilder qb = matchQuery("filename", filename);
    	SearchResponse response = client.prepareSearch(myindex)
    									.setQuery(qb)
/*    									.setHighlighterNumOfFragments(0)*/ //this allow to return full text content with the highlight tag
    									.setSize(100).execute().actionGet();
    	
    	
    	if (response.getHits().getTotalHits() == 0 ){
    		result = true;
    	}else {
    		result = false;
    	}
    	
    	return result;
	}
}
