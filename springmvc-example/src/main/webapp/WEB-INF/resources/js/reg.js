$(document).ready(function() {
    
    var navListItems = $('ul.setup-panel li a'),
        allWells = $('.setup-content');

    allWells.hide();

    navListItems.click(function(e)
    {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this).closest('li');
        
        if (!$item.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show();
        }
        GenerateConfirmPageValue();
    });
    
    $('ul.setup-panel li.active a').trigger('click');
//    $(".form-group").removeClass("has-error");
    // DEMO ONLY //
    $('#activate-step-2').on('click', function(e) {
        var isValid = false;
        $(".form-group").removeClass("has-error");
        $(".form-notice li").remove();
        if ($('#username').val().trim() != '' && $('#password').val().trim() != '' && $('#confirmpw').val().trim() != '' ) {
            
            if ($('#password').val() != $('#confirmpw').val()){
                $('.form-notice').append('<li> Confirm Password does not match</li>');
                $('#confirmpw').closest(".form-group").addClass("has-error");
            }else {
                isValid = true;
            }
            
            
        }else {
            var curStep = $(this).closest(".setup-content"),
            curInputs = curStep.find("input[required='required']")
            for(var i=0; i<curInputs.length; i++){
                  if (!curInputs[i].validity.valid){
                      $(curInputs[i]).closest(".form-group").addClass("has-error");
                      $('.form-notice').append('<li> The field '+($(curInputs[i]).parent().find(".control-label").text())+' must be fill in the content</li>');
                  }
              }
            
//            alert("Please fill in the required content");
        }
        
        if (isValid){
            $('ul.setup-panel li:eq(1)').removeClass('disabled');
            $('ul.setup-panel li a[href="#step-2"]').trigger('click');
        }

      /*  $(this).remove();*/
    });
    
    $('#activate-step-3').on('click', function(e) {
        var isValid = false;
        $(".form-group").removeClass("has-error");
        $(".form-notice li").remove();
        if ($('#firstname').val().trim() != '' && $('#lastname').val().trim() != '') {  
            isValid = true; 
        }else {
            var curStep = $(this).closest(".setup-content"),
            curInputs = curStep.find("input[required='required']")
            for(var i=0; i<curInputs.length; i++){
                  if (!curInputs[i].validity.valid){
                      $(curInputs[i]).closest(".form-group").addClass("has-error");
                      $('.form-notice').append('<li> The field '+($(curInputs[i]).parent().find(".control-label").text())+' must be fill in the content</li>');
                  }
              }
            
//            alert("Please fill in the required content");
        }
        
        if (isValid){
            $('ul.setup-panel li:eq(2)').removeClass('disabled');
            $('ul.setup-panel li a[href="#step-3"]').trigger('click');
        }
        
      /*  $(this).remove();*/
        GenerateConfirmPageValue();

    });
    
    
    function GenerateConfirmPageValue(){
        $("#confirm-user-name").text($('#username').val().trim());
        $("#confirm-email").text($('#email').val().trim());
        $("#confirm-last-name").text($('#lastname').val().trim());
        $("#confirm-first-name").text($('#firstname').val().trim());
        $("#confirm-gender").text($('input[name=optionsGender]:checked').val());
    }

    
   $('div.setup-panel div a.btn-primary').trigger('click'); 
});