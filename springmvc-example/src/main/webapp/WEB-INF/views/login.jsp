<%@include file="/WEB-INF/views/template/reg-login-header.jsp" %>
    
<!-- Main Content
================================================== --> 



    <h2 class="text-center label-login-form">Login Form</h2>
	
    <form name="loginForm" class="form-signin" role="form" action="<c:url value="/j_spring_security_check" />" method="post">
      <div class="imgcontainer">
        <img src="<c:url value="/resources/images/img_avatar2.png"/>" alt="Avatar" class="avatar">
      </div>
      
		<c:if test="${not empty msg}">
			<div class="msg">${msg}</div>
		</c:if>
	
      <div class="container login-container">
        
        <div class="form-group row">
            <label><b>Username </b></label>
            <input type="text" class="login-form-input" id="username" placeholder="Enter Username" name="username" required>
        </div>

        <div class="form-group row">
            <label><b>Password </b></label>
            <input type="password" class="login-form-input" id="password" placeholder="Enter Password" name="password" required>
        </div>
        <div class="form-group row">
            <input type="checkbox" checked="checked"> Remember me
        </div>
        <div class="form-group row">
            <button type="submit">Login</button>
			<c:if test="${not empty error}">
				<div class="error" style="color: #ff0000;">${error}</div>
			</c:if>
        </div>
      </div>

        <div class="from-group-help" style="background-color:#f1f1f1">
            <button type="button" class="cancelbtn">Cancel</button>
            <span class="psw">Forgot <a href="#">password?</a></span>
        </div>
        
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>



<%@include file="/WEB-INF/views/template/reg-login-footer.jsp" %>
