<%@include file="/WEB-INF/views/template/reg-login-header.jsp" %>
    
<!-- Main Content
================================================== -->       
    
    <!--  Body  -->
    <div class="container">
        <h1 class="text-center label-reg-form">Registration Form</h1>
        <div class="row form-group">
            <div class="col-xs-12">
                <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                    <li class="active"><a href="#step-1">
                        <h4 class="list-group-item-heading">Step 1 <span class="glyphicon glyphicon-chevron-right"></span></h4>
                        <p class="list-group-item-text">Account Information</p>
                    </a></li>
                    <li class="disabled"><a href="#step-2">
                        <h4 class="list-group-item-heading">Step 2 <span class="glyphicon glyphicon-chevron-right"></span></h4>
                        <p class="list-group-item-text">Personal Information</p>
                    </a></li>
                    <li class="disabled"><a href="#step-3">
                        <h4 class="list-group-item-heading">Step 3</h4>
                        <p class="list-group-item-text">Confirmation</p>
                    </a></li>
                </ul>
            </div>
        </div>

        <form role="form" action="" method="post">

            <div class="row setup-content" id="step-1">
                <div class="col-xs-12">
                    <div class="col-md-12 well">
                        <h3 class="text-center step-header">Account Information</h3>
                            <ul class="form-notice"></ul>

                            <div class="form-group">
                                <label class="control-label">User Name</label>
                                <input id="username" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter User Name"  />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Password</label>
                                <input id="password" maxlength="100" type="password" required="required" class="form-control" placeholder="Enter Password" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Confirm Password</label>
                                <input id="confirmpw" maxlength="100" type="password" required="required" class="form-control" placeholder="Re Enter Password" />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Email Address</label>
                                <input id="email" class="form-control" placeholder="Enter your email address" />
                            </div>
                        <button id="activate-step-2" type="button" class="btn btn-primary nextBtn btn-lg">Next</button>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-2">
                <div class="col-xs-12">
                    <div class="col-md-12 well">
                        <h3 class="text-center step-header">Personal Information</h3>
                        <ul class="form-notice"></ul>
                        <div class="form-group">
                            <label class="control-label">First Name</label>
                            <input id="firstname" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter First Name"  />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Last Name</label>
                            <input id="lastname" maxlength="100" type="text" required="required" class="form-control" placeholder="Enter Last Name"  />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Gender</label>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" name="optionsGender" id="optionsRadios1" value="male">
                                Male    
                            </div>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" name="optionsGender" id="optionsRadios2" value="female">
                                Female
                            </div>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" name="optionsGender" id="optionsRadios3" value="neutral" checked>
                                Neutral
                            </div>
                        </div>
                        <button id="activate-step-3" type="button" class="btn btn-primary nextBtn btn-lg">Next</button>
                    </div>

                </div>
            </div>
            <div class="row setup-content" id="step-3">
                <div class="col-xs-12">
                    <div class="col-md-12 well">
                        <h3 class="text-center step-header">Confirmation Details</h3>
                        <div class="confirm-form-group">
                            <label class="control-label">User Name: </label>
                            <span id="confirm-user-name">Nil</span>
                        </div>
                        <div class="confirm-form-group">
                            <label class="control-label">Email Address: </label>
                            <span id="confirm-email">Nil</span>
                        </div>
                        <div class="confirm-form-group">
                            <label class="control-label">Last Name: </label>
                            <span id="confirm-last-name">Nil</span>
                            <label class="control-label label-first-name">First Name: </label>
                            <span id="confirm-first-name">Nil</span>
                        </div>
                        <div class="confirm-form-group">
                            <label class="control-label">Gender: </label>
                            <span id="confirm-gender">Nil</span>
                        </div>
                        <button id="submit" type="submit" class="btn btn-primary btn-submit btn-lg">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    
    
<%@include file="/WEB-INF/views/template/reg-login-footer.jsp" %>