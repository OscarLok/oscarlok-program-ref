<%@include file="/WEB-INF/views/template/header.jsp" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!-- Main Content
================================================== -->
    <!-- Page Content -->
    <div class="container">

        <div class="row">

		<!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4 sidebar-margin-top">

                <!-- Blog Search Well -->
                <div class="search-form">
                <form:form method="GET" action="${pageContext.request.contextPath}/main/query">
                    <h4 class="search-title-label">Pdf Content Search</h4>
                    <div class="input-group search-group">
                    	<div class="form-group col-lg-12">
	                        <label>Patient Name</label>
	                        <input type="" name="" class="form-control" id="" value="Chan Tai Man" readonly>
	                    </div>
	
	                    <div class="form-group col-lg-12">
	                        <label>HKID</label>
	                        <input type="" name="" class="form-control" id="" value="Y234567(8)" readonly>
	                    </div>
	
	                    <div class="form-group col-lg-12">
	                        <label>Search Type</label>
		                    <select id="selection" class="selectpicker form-control" data-style="btn-info">
		                      <option value="single">Single Word</option>
		                      <option value="full">Full Sentence</option>
		                    </select>
	                    </div>
	
	                    <div class="form-group col-lg-12">
	                        <label>Search Key</label>
	                        <div class="input-group">
	                            <input name="search" type="text" class="form-control" />
	                            <span class="input-group-btn">
	                            <button class="btn btn-md btn-success" type="submit" value="submit" >Search</button>
	                        </span>
	                        </div>
	                    </div>
                    
                    </div>
                    <!-- /.input-group -->
                </form:form>
                </div>

                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Categories Filter</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.col-lg-6 -->
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                                <li><a href="#">Category Name</a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.col-lg-6 -->
                    </div>
                    <!-- /.row -->
                </div>

            </div>


            <!-- Blog Entries Column -->
            <div class="col-md-8">
            	
            	<c:if test="${pageContext.request.userPrincipal.name != null}">
	                <h1 class="page-header">
	                    Welcome! <small>${pageContext.request.userPrincipal.name} | <a href="<c:url value="/j_spring_security_logout" />">Logout</a></small>
	                </h1>
				</c:if>
				
				<table class="table table-striped">
				<thread>
					<tr>
						<th class="search-table-title">Search Result <a href="#" class= "read-more-button">Sort by Date <span class="glyphicon glyphicon-sort-by-attributes search-sort-icon"></span></a></th>
					</tr>
				</thread>
					
					<c:forEach items="${pdfcontent}" var="pdfcontent">
					<tr>
					<td>
						<div>
		                <!-- First Blog Post -->
		                <h4>
		                    <a href="#">${pdfcontent.filename}</a>
		                </h4>
		                <p><span class="glyphicon glyphicon-time"></span> Created on ${pdfcontent.date}
		                <a class="btn btn-primary read-more-button" >Read More <span class="glyphicon glyphicon-chevron-down"></span></a>
		                </p>
		                
		                <p class="highlight">${pdfcontent.highlightfield}</p>
						<hr>
		
						</div>
					</td>
					</tr>
					</c:forEach>
				</table>
				
				
                <!-- Pager -->
<!--                 <ul class="pager">
                    <li class="previous">
                        <a href="#">&larr; Older</a>
                    </li>
                    <li class="next">
                        <a href="#">Newer &rarr;</a>
                    </li>
                </ul> -->

            </div>


        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- MainPage JS -->
    

<%@include file="/WEB-INF/views/template/footer.jsp" %>