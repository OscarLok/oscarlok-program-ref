    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="footer-row row">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li>
                            <a href="#">Home</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="#about">About</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="#services">Services</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="#contact">Contact</a>
                        </li>
                    </ul>
                    <p class="copyright text-muted small">Copyright &copy; OscarLok 2017. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="<c:url value="/resources/js/jquery.min.js"/>" ></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>" ></script>
    
    <!-- MainPage JS -->
    <script src="<c:url value="/resources/js/mainpage.js"/>" ></script>
	
	<!-- Own Js  -->
<%-- 	<script src="<c:url value="/resources/js/freelancer.js"/>" ></script> --%>
</body>

</html>