<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Author: Oscar Lok
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <title><c:out value="${PageTitle}"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet" id="bootstrap-css">
    
    <link href="<c:url value="/resources/css/reg-login-layout.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery-1.10.2.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>

</head>

<body>
    
    <!--  Header  -->
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="nav-header">
          <a class="navbar-brand" href="#"><img class="img-responsive img-header-ha" src="<c:url value="/resources/images/Hospital_Authority.png"/>"></a>
        </div>
        <ul class="nav navbar-nav">
            <li class="reg-nav"><a href="<c:url value="/" />" >Home</a></li>
            <c:choose> 
			  <c:when test="${PageTitle == 'Registration Form'}">
			   	<li class="reg-nav"><a href="<c:url value="/login" />" >Sign in</a></li>
			  </c:when>
			  <c:otherwise>
			    <li class="reg-nav"><a href="<c:url value="/registration" />" >Sign up</a></li>
			  </c:otherwise>
			</c:choose>
            
            
        </ul>
      </div>
    </nav>