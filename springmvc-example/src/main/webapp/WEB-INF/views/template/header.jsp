<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Author: Oscar Lok
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Landing Page - R&D</title>

    <!-- Bootstrap Core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<c:url value="/resources/css/landing-page.css"/>" rel="stylesheet">
    
    <link href="<c:url value="/resources/css/layout.css"/>" rel="stylesheet">
    
    <%--Data Table--%>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel="stylesheet">
	
    <!-- Custom Fonts -->
    <link href="<c:url value="/resources/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">


</head>

<body>

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav" href="<c:url value="/" />"><img class="img-responsive img-header-ha" src="<c:url value="/resources/images/Hospital_Authority2.png"/>" alt=""></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right ">
                    <li class="page-scroll">
                        <a href="#about" style="font-size:16px">About</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#services" style="font-size:16px">Services</a>
                    </li>
                    <li class="page-scroll">
                        <a href="<c:url value="/main" />" style="font-size:16px">Main</a>
                    </li>
                    <li class="dropdown">
                    	<a class="dropdown-toggle login-dropdown" data-toggle="dropdown" href="#">
                    	<c:if test="${pageContext.request.userPrincipal.name != null}">
                    		${pageContext.request.userPrincipal.name}
                    	</c:if>
                    	<c:if test="${pageContext.request.userPrincipal.name  == null}">
                    		Sign in
                    	</c:if>
                    	 <span class="caret"></span>
                    	</a>
	                    <ul class="dropdown-menu">
	                        <c:if test="${pageContext.request.userPrincipal.name != null}">
	                            <li><a>Welcome: ${pageContext.request.userPrincipal.name}</a></li>
	                            <li><a href="<c:url value="/j_spring_security_logout" />">Logout</a></li>
	                        </c:if>
	                        <c:if test="${pageContext.request.userPrincipal.name  == null}">
			                    <li><a href="<c:url value="/login" />" style="font-size:16px">Sign in</a></li>
			                    <li><a href="<c:url value="/registration" />">Register</a></li>
			                </c:if>
	                    </ul>
	                 </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    
    <h1 class="header-margin-adjust"></h1>