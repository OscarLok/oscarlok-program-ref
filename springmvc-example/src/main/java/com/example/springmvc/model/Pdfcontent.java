package com.example.springmvc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pdfcontent {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String Id;
	

	private String patient;
	private String filename;
	@JsonProperty("HKID")
	private String hkid;
	private String date;
	
	private String highlightfield;

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getPatient() {
		return patient;
	}

	public void setPatient(String patient) {
		this.patient = patient;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getHighlightfield() {
		return highlightfield;
	}

	public void setHighlightfield(String highlightfield) {
		this.highlightfield = highlightfield;
	}
}
