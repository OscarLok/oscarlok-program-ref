package com.example.springmvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import org.apache.log4j.Logger;

import com.example.springmvc.model.Pdfcontent;
import com.example.springmvc.service.PdfsearchService;

@Controller
@RequestMapping("/main")
public class MainController {
	
	@Autowired
	private PdfsearchService pdfsearchservice;
	private static final Logger LOGGER = Logger.getLogger(MainController.class.getName());
	private static final String searchindex = "myindex_v6";
	
	@RequestMapping(value="/query", method = RequestMethod.GET)
	public String pdfSearch(@RequestParam("search") String searchKey, Model model){
		
		LOGGER.info(searchKey+" testing...");
		List<Pdfcontent> result = pdfsearchservice.getPdfcontentByQuery(searchKey, searchindex);
//		LOGGER.info("Result: "+result.toString());
		if(result ==null){
			LOGGER.info("Test No record found...");
		}
		model.addAttribute("pdfcontent", result);
		
		return "main";
	}
	
}
