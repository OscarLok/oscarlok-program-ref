package com.example.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String homePage(){
		
		return "home";
	}
	
	@RequestMapping("/main")
	public String mainPage(){
		
		return "main";
	}
	
	@RequestMapping("/login")
	public String loginPage(@RequestParam(value="error", required = false) String error, @RequestParam(value="logout",
            required = false) String logout, @RequestParam(value="timeout", required = false) String timeout, Model model) {
        
		model.addAttribute("PageTitle", "Login Form");
		
		if (error!=null) {
            model.addAttribute("error", "Invalid username and password");
        }

        if(logout!=null) {
            model.addAttribute("msg", "You have been logged out successfully.");
        }
        
        if(timeout!=null) {
        	model.addAttribute("msg", "Your session have been timeout.");
        }

        return "login";
    }
	
	@RequestMapping("/registration")
	public String registrationPage(Model model){
		
		model.addAttribute("PageTitle", "Registration Form");
		
		return "registration";
	}
}
