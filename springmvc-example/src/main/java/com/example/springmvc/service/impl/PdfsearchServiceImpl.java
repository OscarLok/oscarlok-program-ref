package com.example.springmvc.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
 



import org.apache.log4j.Logger;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.example.springmvc.model.Pdfcontent;
import com.example.springmvc.service.PdfsearchService;

@Service
public class PdfsearchServiceImpl implements PdfsearchService {

	private static final Logger LOGGER = Logger.getLogger(PdfsearchServiceImpl.class.getName());
	public static final String REST_SERVICE_URI = "http://localhost:8080/ws-example/pdfcontent";
	
	public List<Pdfcontent> getPdfcontentByQuery(String keyword, String index ){
		LOGGER.info("Calling the pdfsearch Rest API services...");

		ResponseEntity<Pdfcontent[]> responseEntity = new ResponseEntity<Pdfcontent[]>(null);
		RestTemplate restTemplate = new RestTemplate();
		
		// Add the Jackson message converter
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		
		try{
		responseEntity = restTemplate.getForEntity(REST_SERVICE_URI+"/query?index="+index+"&search="+keyword, Pdfcontent[].class);
		} catch (HttpClientErrorException e){
			e.printStackTrace();
		}
		Pdfcontent[] pdfcontents = responseEntity.getBody();
		
		if (pdfcontents != null){
			LOGGER.info(pdfcontents[0].getFilename());
			return Arrays.asList(pdfcontents);
		}else {
			LOGGER.info("No PDF record found.");
			return null;
		}
		
/*		try{
		List<LinkedHashMap<String, Object>> pdfMap = restTemplate.getForObject(REST_SERVICE_URI+"/query?index="+index+"&search="+keyword, List.class);
		if(pdfMap != null){
			for(LinkedHashMap<String, Object> map : pdfMap){
				LOGGER.info("PDF Record : id="+map.get("id")+", File Name="+map.get("filename")+", date="+map.get("date")+", highlight fields="+map.get("highlightfield"));
			}
		}else {
			LOGGER.info("No PDF record found.");
		}
		} catch (Exception e){
			e.printStackTrace();
		}*/
		
	} 
}
