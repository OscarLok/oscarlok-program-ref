package com.example.springmvc.service;

import java.util.List;

import com.example.springmvc.model.Pdfcontent;

public interface PdfsearchService {
//	Pdfcontent getPdfcontentById(String id);
    
    List<Pdfcontent> getPdfcontentByQuery(String keyword, String index );
}
