Java Elasticsearch Project Template
===================================

The project is separated to two part. First is the Elasticsearch micro-services, the other is the spring mvc frontend (Just for demonstration).
- If you want to look at the key coding, you can just open the ws-example folder or only clone the ws-example folder to your machine
- If you want to make the entire project work in your machine, you must launch the elasticsearch server with version 2.4.1 and also need to create a index and mapping properties in elasticsearch

Requirements for build up the project
-------------------------------------
- install elasticsearch
    - on any platform, download and unzip the folder [Elasticsearch@2.4.1](https://www.elastic.co/downloads/past-releases/elasticsearch-2-4-1)
    - install the mapper-attachment plugin for elasticsearch [mapper-attachments@2.4.6](https://www.elastic.co/guide/en/elasticsearch/plugins/2.4/mapper-attachments.html)

- Clone this repo

- Download [Postman](https://www.getpostman.com/) for testing the API

- Extract the pdf content and encode by base64 (As this project did not handle the data access part) or just testing by using `IkdvZCBTYXZlIHRoZSBRdWVlbiIgKGFsdGVybmF0aXZlbHkgIkdvZCBTYXZlIHRoZSBLaW5nIg==`


Getting started (Elasticsearch setup part)
------------------------------------------

- Open a terminal (command line) window

- Type `cd C:\elasticsearch-2.4.1\bin` (the path is depends on your where you install in your machine)

- Start the elasticsearch server
    - on Window, type `elasticsearch.bat`
    - on Linux, type `elasticsearch.sh`

- Create an index in elasticsearch
    - Open the Postman and type `localhost:9200` in the Url field
    - Make a PUT request to create an index by typing `PUT localhost:9200/test_v1` (You can define your own index name like test_v1)
    - Set the mapping properties for searching `PUT /test_v1/pdfcontent/_mapping` with json content:
```
"pdfcontent": {
    "properties": {
        "HKID": {
            "type": "string"
        },
        "date": {
            "type": "date",
            "format": "strict_date_optional_time||epoch_millis"
        },
        "file": {
            "type": "attachment",
            "fields": {
                "content": {
                    "type": "string",
                    "store": true,
                    "term_vector": "with_positions_offsets"
                },
                "author": {
                    "type": "string"
                },
                "title": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "date": {
                    "type": "date",
                    "format": "strict_date_optional_time||epoch_millis"
                },
                "keywords": {
                    "type": "string"
                },
                "content_type": {
                    "type": "string"
                },
                "content_length": {
                    "type": "integer"
                },
                "language": {
                    "type": "string"
                }
            }
        },
        "filename": {
            "type": "string"
        },
        "patient": {
            "type": "string"
        }
    }
}
```

Getting started (The webservices part)
------------------------------------------
- Test the API function

- Add pdf to the Elasticsearch by using the API
    - Build and launch the ws-example project to Tomcat@7.0
    - Open the Postman to select as a POST request
    - Type `localhost:8080/ws-example/pdfcontent/add?index=test_v10&type=pdfcontent` in the Url field
    - Type the Body field with json format like:
```
[
	{
		"filename": "example.pdf",
		"patient": "testing",
		"file": "IkdvZCBTYXZlIHRoZSBRdWVlbiIgKGFsdGVybmF0aXZlbHkgIkdvZCBTYXZlIHRoZSBLaW5nIg==",
		"HKID": "Y1234567"
	}
]
```
-  Search the pdfcontent by using the API
    - Make a GET request in Postman by typing `localhost:8080/ws-example/pdfcontent/query?index=test_v1&search=king`
    - The result will be return successfully like:
```
[
    {
        "patient": "oscar testing",
        "filename": "example.pdf",
        "date": null,
        "highlightfield": "\"God Save the Queen\" (alternatively \"God Save the <em>King</em>\"\n",
        "id": "AV1871g2IUvdfY3QTiUj",
        "HKID": "Y1234567"
    }
]
```